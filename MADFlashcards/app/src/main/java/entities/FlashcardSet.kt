package entities

data class FlashcardSet(val title: String){


    fun qna(): MutableList<FlashcardSet> {
        val title1 = FlashcardSet("Lakers")
        val title2 = FlashcardSet("Bucks")
        val title3 = FlashcardSet("Warriors")
        val title4 = FlashcardSet("Dark Knight")
        val title5 = FlashcardSet( "Man of Steel")
        val title6 = FlashcardSet("Emerald Warrior")
        val title7 = FlashcardSet("Super Soldier")
        val title8 = FlashcardSet("Spider")
        val title9 = FlashcardSet("Best there is at what he does")
        val title10 = FlashcardSet("Scarlet Speedster")

        val lst2 = mutableListOf<FlashcardSet>()

        lst2.add(title1)
        lst2.add(title2)
        lst2.add(title3)
        lst2.add(title4)
        lst2.add(title5)
        lst2.add(title6)
        lst2.add(title7)
        lst2.add(title8)
        lst2.add(title9)
        lst2.add(title10)

        return lst2
    }
}
