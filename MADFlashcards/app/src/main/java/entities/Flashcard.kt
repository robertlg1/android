package entities

data class Flashcard(val term: String, var definition: String = "") {


    fun fc(): MutableList<Flashcard> {
        val term1 = Flashcard("Lebron James")
        val term2 = Flashcard("Giannis Antetokounmpo")
        val term3 = Flashcard("Stephen Curry")
        val term4 = Flashcard("Bruce Wayne")
        val term5 = Flashcard("Clark Kent")
        val term6 = Flashcard("Hal Jordan")
        val term7 = Flashcard("Steve Rogers")
        val term8 = Flashcard("Ben Reily")
        val term9 = Flashcard("James Howlett")
        val term10 = Flashcard("Wally West")
        term1.definition = "The chosen one"
        term2.definition = "Greek Freak"
        term3.definition = "Baby faced Assassin"
        term4.definition = "Batman"
        term5.definition = "Superman"
        term6.definition = "Green Lantern"
        term7.definition = "Captain America"
        term8.definition = "Spider-Man"
        term9.definition = "Wolverine"
        term10.definition = "The Flash"


        val lst2 = mutableListOf<Flashcard>()


        lst2.add(term1)
        lst2.add(term2)
        lst2.add(term3)
        lst2.add(term4)
        lst2.add(term5)
        lst2.add(term6)
        lst2.add(term7)
        lst2.add(term8)
        lst2.add(term9)
        lst2.add(term10)


        return lst2


    }
}